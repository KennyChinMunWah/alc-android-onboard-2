package com.example.alc_android_onboard2.adapters;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.Database.Entities.MyFriends;
import com.example.alc_android_onboard2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyFriendsAdapter extends RecyclerView.Adapter<MyFriendsAdapter.ViewHolder> {
    private ArrayList<MyFriends> mData;
    private Callbacks mCallbacks;
    private static Context mContext;

    public MyFriendsAdapter(ArrayList<MyFriends> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.items_my_friend,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position),mCallbacks);
    }

    @Override
    public int getItemCount() { return mData.size(); }

    public void setData(ArrayList<MyFriends> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.constraint_layout)
        public ConstraintLayout root;

        @BindView(R.id.TextView_firstname)
        public TextView firstname;

        @BindView(R.id.TextView_lastname)
        public TextView lastname;

        @BindView(R.id.TextView_Gmail)
        public TextView eamil;

        @BindView(R.id.imageView2)
        public ImageView avatar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bindTo(MyFriends data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                //set data
                firstname.setText(data.first_name);
                lastname.setText(data.last_name);
                eamil.setText(data.email);
                Glide.with(mContext)
                        .load(data.avatar)
                        .into(avatar);

                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });

                }
            }
        }
        public void resetViews() {
            firstname.setText("");
            lastname.setText("");
            eamil.setText("");
            Glide.with(mContext)
                    .load("")
                    .into(avatar);
            root.setOnClickListener(null);
        }
    }

    public interface Callbacks{
        void onListItemClicked(MyFriends data);
    }
}
