package com.example.alc_android_onboard2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.alc_android_onboard2.BuildConfig;
import com.example.alc_android_onboard2.R;
import com.example.alc_android_onboard2.activities.MainActivity;
import com.example.alc_android_onboard2.activities.TermsOfUseActivity;
import com.example.alc_android_onboard2.gsonmodels.LoginModel;
import com.example.alc_android_onboard2.utils.NetworkHelper;
import com.example.alc_android_onboard2.utils.SharedPreferenceHelper;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.textView2)
    protected TextView mTextView;

    @BindView(R.id.Text_Email)
    protected EditText EditTextEmail;

    @BindView(R.id.Text_Password)
    protected EditText EditTextPassword;

    @BindView(R.id.button_login)
    protected Button ButtonLogin;

    private Unbinder mUnbinder;

    public LoginFragment() {
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @OnClick(R.id.textView2)
    public void goTermsAct() {
        startActivity(new Intent(getActivity(), TermsOfUseActivity.class));
    }

    @OnClick(R.id.button_login)
    protected void doLogin() {

        ButtonLogin.setEnabled(false);
        ButtonLogin.setText("Loading");

        final String email = EditTextEmail.getText().toString().trim();
        final String password = EditTextPassword.getText().toString();

        if (email.equals("") || password.equals("")) {
            new MaterialDialog.Builder(getActivity())
                    .title("Error")
                    .content("Do not Leave it Blank.")
                    .positiveText("OK")
                    .show();
            ButtonLogin.setText("Login");
            ButtonLogin.setEnabled(true);
            return;
        }

        //Call login API
        String url = BuildConfig.BASE_API_URL + "login";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Login success.", Toast.LENGTH_SHORT).show();
                if (getLifecycle().getCurrentState().toString().compareToIgnoreCase("RESUMED") == 0) {
                    // Convert JSON string to Java object.
                    LoginModel responseModel = new GsonBuilder().create().fromJson(response, LoginModel.class);

                    // Save user's email to shared preference.
                    SharedPreferenceHelper.getInstance(getActivity())
                            .edit()
                            .putString("email", email)
                            .putString("token", responseModel.token)
                            .apply();
                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (getLifecycle().getCurrentState().toString().compareToIgnoreCase("RESUMED") == 0) {
                    // Show error in popup dialog.
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("Invalid email or password.")
                            .positiveText("OK")
                            .show();
                }
                ButtonLogin.setText("Login");
                ButtonLogin.setEnabled(true);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
    }
}
