package com.example.alc_android_onboard2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.alc_android_onboard2.BuildConfig;
import com.example.alc_android_onboard2.R;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class Friends_DetailsFragment extends Fragment {
    public static final String TAG = TermsFragment.class.getSimpleName();
    private static final String ARG_INT_MY_ID = "ARG_INT_MY_ID";

    private Unbinder mUnbinder;
    private int mId = 0;

    @BindView(R.id.profile_image)
    protected ImageView avatar;

    @BindView(R.id.TextView_first_name)
    protected TextView firstname;

    @BindView(R.id.TextView_last_name)
    protected TextView lastname;

    @BindView(R.id.TextView_email)
    protected TextView email;

    public Friends_DetailsFragment() {
    }

    public static Friends_DetailsFragment newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt(ARG_INT_MY_ID, id);
        Friends_DetailsFragment fragment = new Friends_DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_friends__details, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            mId = args.getInt(ARG_INT_MY_ID, 0);
        }
        initView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    private void initView() {
        if (mId > 1) {
            String url = BuildConfig.BASE_API_URL + "users/" + mId;
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject jsonObject = response.getJSONObject("data");
                        String getfirstname = jsonObject.getString("first_name");
                        String getlastname = jsonObject.getString("last_name");
                        String getavatar = jsonObject.getString("avatar");
                        String getemail = jsonObject.getString("email");
                        firstname.setText(getfirstname);
                        lastname.setText(getlastname);
                        email.setText(getemail);
                        Glide.with(getContext())
                                .load(getavatar)
                                .into(avatar);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), "CANT GET", Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(jsonObjectRequest);
        }
    }
}
