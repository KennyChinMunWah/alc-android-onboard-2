package com.example.alc_android_onboard2.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import com.example.alc_android_onboard2.R;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TermsFragment extends Fragment {

    public static final String TAG = TermsFragment.class.getSimpleName();

    private Unbinder mUnbinder;

    public TermsFragment() {
    }

    public static TermsFragment newInstance() { return new TermsFragment(); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terms, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }
}
