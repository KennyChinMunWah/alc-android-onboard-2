package com.example.alc_android_onboard2.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.Database.Entities.MyFriends;
import com.example.alc_android_onboard2.BuildConfig;
import com.example.alc_android_onboard2.R;
import com.example.alc_android_onboard2.activities.Friends_DetailsActivity;
import com.example.alc_android_onboard2.activities.LoginActivity;
import com.example.alc_android_onboard2.adapters.MyFriendsAdapter;
import com.example.alc_android_onboard2.utils.SharedPreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainFragment extends Fragment implements MyFriendsAdapter.Callbacks {

    public static final String TAG = MainFragment.class.getSimpleName();
    private final int REQUEST_CODE_MY_FRIEND_DETAIL = 300;
    private static String JsonURL = BuildConfig.BASE_API_URL + "users?page=2";

    private Unbinder mUnbinder;
    private MyFriendsAdapter mAdapter;
    ArrayList<MyFriends> mData;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = new ArrayList<>();
        GetData getData = new GetData();
        getData.execute();

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //initView();
        //initViewModel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.menu_logout) {
            new MaterialDialog.Builder(getActivity())
                .title("Logout")
                .content("Are you sure to log Out ? ")
                .positiveText("OK")
                .negativeText("Cancel")
                .onPositive((dialog, which) -> {
                    SharedPreferenceHelper.getInstance(getActivity())
                            .edit()
                            .clear()
                            .apply();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                })
                .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClicked(MyFriends data) {
        //Toast.makeText(getActivity(), "his name is " + data.getFirst_name(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity(), Friends_DetailsActivity.class);
        intent.putExtra(Friends_DetailsActivity.EXTRA_INT_MY_ID,data.getId());
        startActivityForResult(intent,REQUEST_CODE_MY_FRIEND_DETAIL);
    }

    private class GetData extends AsyncTask<String,String,String>{

        @Override
        protected String doInBackground(String... strings) {
            String current ="";
            try{
                URL url;
                HttpURLConnection urlConnection = null;
                try{
                    url = new URL(JsonURL);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream is = urlConnection.getInputStream();
                    InputStreamReader isr = new InputStreamReader(is);

                    int data = isr.read();
                    while(data!=-1){
                        current +=(char) data;
                        data = isr.read();
                    }
                    return current;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }finally{
                    if(urlConnection!=null){
                        urlConnection.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return current;
        }
        @Override
        protected  void onPostExecute(String s){
            super.onPostExecute(s);
            try{
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for(int i=0;i<jsonArray.length();i++){
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    MyFriends myFriends = new MyFriends();
                    myFriends.setId(jsonObject1.getInt("id"));
                    myFriends.setFirst_name(jsonObject1.getString("first_name"));
                    myFriends.setLast_name(jsonObject1.getString("last_name"));
                    myFriends.setEmail(jsonObject1.getString("email"));
                    myFriends.setAvatar(jsonObject1.getString("avatar"));
                    mData.add(myFriends);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            PutDataIntoRecyclerView(mData);
        }
    }

    private void PutDataIntoRecyclerView(ArrayList<MyFriends>mData){
        mAdapter = new MyFriendsAdapter(mData);
        mAdapter.setCallbacks(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);
    }
}
