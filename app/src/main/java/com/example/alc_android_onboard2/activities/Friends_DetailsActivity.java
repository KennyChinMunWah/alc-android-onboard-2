package com.example.alc_android_onboard2.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.alc_android_onboard2.R;
import com.example.alc_android_onboard2.fragments.Friends_DetailsFragment;

import butterknife.ButterKnife;

public class Friends_DetailsActivity extends AppCompatActivity {

    public static final String EXTRA_INT_MY_ID = "EXTRA_INT_MY_ID";
    public static final int RESULT_CONTENT_MODIFIED = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends__details);
        ButterKnife.bind(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.findFragmentByTag(Friends_DetailsFragment.TAG) == null) {
            Intent extra = getIntent();
            int id = 0;
            if (extra != null) {
                id = extra.getIntExtra(EXTRA_INT_MY_ID, 0);
            }
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, Friends_DetailsFragment.newInstance(id), Friends_DetailsFragment.TAG)
                    .commit();
        }
    }
}
