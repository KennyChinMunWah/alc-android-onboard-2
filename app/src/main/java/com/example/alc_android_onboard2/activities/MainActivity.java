package com.example.alc_android_onboard2.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;

import com.example.alc_android_onboard2.R;
import com.example.alc_android_onboard2.fragments.MainFragment;
import com.example.alc_android_onboard2.utils.SharedPreferenceHelper;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        // Check user is logged in.
        if (!SharedPreferenceHelper.getInstance(this).contains("token")) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        } else {
            // User is logged in.
            FragmentManager fragmentManager = getSupportFragmentManager();

            if (fragmentManager.findFragmentByTag(MainFragment.TAG) == null) {
                // Init fragment.
                fragmentManager.beginTransaction()
                        .replace(R.id.framelayout_fragment_holder, MainFragment.newInstance(), MainFragment.TAG)
                        .commit();
            }
        }
    }

}
